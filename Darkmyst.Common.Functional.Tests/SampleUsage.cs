﻿using System;
using System.IO;
using Darkmyst.Common.Functional.Results;

namespace Darkmyst.Common.Functional.Tests
{
    public static class SampleUsage
    {
        public static void FakeMicroserviceListener(BrokeredMessage brokeredMessage, TextWriter log)
        {
            Handling.WithCommonHandling(() => DoSomeWork(brokeredMessage, ParseMessage, GetSomeData));
        }

        private static IResult DoSomeWork(BrokeredMessage message, Func<BrokeredMessage, MyMessageType> parseMessage, Func<int, int, int> getSomeData)
        {
            var myMessage = parseMessage(message);
            return DataResult.CreateSuccessResult(getSomeData(myMessage.X, myMessage.Y));
        }

        private static MyMessageType ParseMessage(BrokeredMessage message)
        {
            return new MyMessageType();
        }

        private static int GetSomeData(int x, int y)
        {
            return x + y;
        }
    }



    public class MyMessageType
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
    public class BrokeredMessage { }
}
