﻿using System;
using System.Linq;
using Xunit;

namespace Darkmyst.Common.Functional.Tests
{
    public class MaybeTests
    {
        [Fact]
        public void EmptyMaybeHasNoValue()
        {
            var actual = Maybe.Empty<int>();
            Assert.False(actual.HasValue);
        }
        [Fact]
        public void EmptyMaybeIsEmpty()
        {
            var actual = Maybe.Empty<int>();
            Assert.Empty(actual);
        }
        [Fact]
        public void NewMaybeWithoutValueIsEmptyMaybe()
        {
            Assert.Equal(Maybe.Empty<int>(), new Maybe<int>());
        }
        [Fact]
        public void NewMaybeWithNullValueIsEmptyMaybe()
        {
            Assert.Equal(Maybe.Empty<string>(), new Maybe<string>(null));
        }
        [Fact]
        public void NonEmptyMaybeHasValue()
        {
            var actual = GenerateRandomInt().ToMaybe();
            Assert.True(actual.HasValue);
        }
        [Fact]
        public void NonEmptyMaybeIsNotEmpty()
        {
            var actual = GenerateRandomInt().ToMaybe();
            Assert.NotEmpty(actual);
        }
        [Fact]
        public void ValueIsAsExpected()
        {
            var expectedValue = GenerateRandomInt();

            Assert.Equal(expectedValue, expectedValue.ToMaybe().Value);
        }
        [Fact]
        public void ValueNotPresent_CountIsZero()
        {
            var actual = Maybe.Empty<int>();

            Assert.Equal(0, actual.Count());
        }
        [Fact]
        public void ValuePresent_CountIsOne()
        {
            var actual = GenerateRandomInt().ToMaybe();

            Assert.Equal(1, actual.Count());
        }
        [Fact]
        public void DifferentMaybesOfSameValueAssertEqual()
        {
            var value = GenerateRandomInt();
            var a = value.ToMaybe();
            var b = value.ToMaybe();
            Assert.Equal(a, b);
        }
        [Fact]
        public void DifferentMaybesOfSameValueOperatorEqual()
        {
            var value = GenerateRandomInt();
            var a = value.ToMaybe();
            var b = value.ToMaybe();
            Assert.True(a == b);
        }
        [Fact]
        public void DifferentMaybesOfSameValueMethodEqual()
        {
            var value = GenerateRandomInt();
            var a = value.ToMaybe();
            var b = value.ToMaybe();
            Assert.True(a.Equals(b));
        }
        [Fact]
        public void DifferentMaybesOfSameValueAsObjectAssertEqual()
        {
            var value = GenerateRandomInt();
            var a = (object)value.ToMaybe();
            var b = (object)value.ToMaybe();
            Assert.True(a.Equals(b));
        }
        [Fact]
        public void DifferentMaybesOfSameValueAsObjectOperatorEqual()
        {
            var value = GenerateRandomInt();
            var a = value.ToMaybe();
            var b = (object)value.ToMaybe();
            Assert.True(a == b);
        }
        [Fact]
        public void DifferentMaybesAssertNotEqual()
        {
            var a = GenerateRandomInt().ToMaybe();
            var b = GenerateRandomInt(a).ToMaybe();
            Assert.NotEqual(a, b);
        }
        [Fact]
        public void DifferentMaybesOperatorNotEqual()
        {
            var a = GenerateRandomInt().ToMaybe();
            var b = GenerateRandomInt(a);
            Assert.True(a != b);
        }
        [Fact]
        public void DifferentMaybesMethodNotEqual()
        {
            var a = GenerateRandomInt().ToMaybe();
            var b = GenerateRandomInt(a);
            Assert.False(a.Equals(b));
        }
        [Fact]
        public void DifferentMaybesAsObjectAssertEqual()
        {
            var a = GenerateRandomInt().ToMaybe();
            var b = GenerateRandomInt(a);
            Assert.True(!((object)a).Equals((object)b));
        }
        [Fact]
        public void DifferentMaybesAsObjectOperatorNotEqual()
        {
            var a = GenerateRandomInt().ToMaybe();
            var b = GenerateRandomInt(a);
            Assert.True(a != (object)b);
        }
        [Fact]
        public void MaybeComparedToSameValueOperatorEqual()
        {
            var value = GenerateRandomInt();
            var a = value.ToMaybe();
            Assert.True(a == value);
        }
        [Fact]
        public void MaybeComparedToDiferentValueOperatorNotEqual()
        {
            var value = GenerateRandomInt();
            var a = GenerateRandomInt(value).ToMaybe();
            Assert.True(a != value);
        }
        [Fact]
        public void ImplicitConversionToMaybeIsSupported()
        {
            var value = GenerateRandomInt();
            Maybe<int> maybe = value;

            Assert.Equal(value, maybe.Value);
        }
        [Fact]
        public void ImplicitConversionFromMaybeIsSupported()
        {
            var maybe = GenerateRandomInt().ToMaybe();
            int value = maybe;

            Assert.Equal(maybe.Value, value);
        }
        [Fact]
        public void MethodsCanReturnMaybeWithoutUsingItInternally()
        {
            var testFunc = new Func<int, Maybe<string>>(x => x.ToString());
            var value = GenerateRandomInt();
            var result = testFunc(value);
            Assert.Equal(value.ToString(), result.Value);
        }
        [Fact]
        public void NullsImplicitlyConvertToEmptyMaybes()
        {
            string value = null;
            Maybe<string> maybe = value;
            Assert.Equal(Maybe.Empty<string>(), maybe);
        }

        private Random random = new Random();
        private int GenerateRandomInt(int? except = null)
        {
            int? value = null;
            do
            {
                value = random.Next();
            } while (value == except);
            return value.Value;
        }
    }
}
