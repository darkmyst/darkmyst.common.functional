using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Darkmyst.Common.Functional
{
    [DebuggerStepThrough]
    public static class MaybeExtensions
    {
        public static Maybe<T> ToMaybe<T>(this T value)
        {
            return new Maybe<T>(value);
        }

        public static Maybe<T> AsMaybe<T>(this T? @this) where T : struct
        {
            return @this?.ToMaybe() ?? Maybe.Empty<T>();
        }

        public static Maybe<T> AsMaybe<T>(this IEnumerable<T> @this)
        {
            return @this.SingleMaybe();
        }

        public static Maybe<T> FirstMaybe<T>(this IEnumerable<T> @this)
        {
            return @this.Select(x => x.ToMaybe()).DefaultIfEmpty(Maybe.Empty<T>()).FirstOrDefault();
        }

        public static Maybe<T> SingleMaybe<T>(this IEnumerable<T> @this)
        {
            return @this.Select(x => x.ToMaybe()).DefaultIfEmpty(Maybe.Empty<T>()).SingleOrDefault();
        }

        public static Maybe<TValue> GetMaybe<TKey, TValue>(this IDictionary<TKey, TValue> @this, TKey key)
        {
            TValue value;
            return @this.TryGetValue(key, out value) ? value.ToMaybe() : Maybe.Empty<TValue>();
        }
    }
}