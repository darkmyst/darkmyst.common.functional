using System;
using System.Globalization;

namespace Darkmyst.Common.Functional
{
    public static partial class ParseMaybe
    {
        public static Maybe<bool> ToBoolean(Func<string> valueFactory)
        {
            return ToBoolean(valueFactory());
        }
        public static Maybe<char> ToChar(Func<string> valueFactory)
        {
            return ToChar(valueFactory());
        }
        public static Maybe<sbyte> ToSByte(Func<string> valueFactory)
        {
            return ToSByte(valueFactory());
        }
        
        public static Maybe<sbyte> ToSByte(Func<string> valueFactory, NumberStyles style, IFormatProvider formatProvider)
        {
            return ToSByte(valueFactory(), style, formatProvider);
        }
        
        public static Maybe<byte> ToByte(Func<string> valueFactory)
        {
            return ToByte(valueFactory());
        }
        public static Maybe<byte> ToByte(Func<string> valueFactory, NumberStyles style, IFormatProvider formatProvider)
        {
            return ToByte(valueFactory(), style, formatProvider);
        }

        public static Maybe<short> ToInt16(Func<string> valueFactory)
        {
            return ToInt16(valueFactory());
        }
        public static Maybe<short> ToInt16(Func<string> valueFactory, NumberStyles style, IFormatProvider formatProvider)
        {
            return ToInt16(valueFactory(), style, formatProvider);
        }

        public static Maybe<ushort> ToUInt16(Func<string> valueFactory)
        {
            return ToUInt16(valueFactory());
        }
        public static Maybe<ushort> ToUInt16(Func<string> valueFactory, NumberStyles style, IFormatProvider formatProvider)
        {
            return ToUInt16(valueFactory(), style, formatProvider);
        }

        public static Maybe<int> ToInt32(Func<string> valueFactory)
        {
            return ToInt32(valueFactory());
        }
        public static Maybe<int> ToInt32(Func<string> valueFactory, NumberStyles style, IFormatProvider formatProvider)
        {
            return ToInt32(valueFactory(), style, formatProvider);
        }

        public static Maybe<uint> ToUInt32(Func<string> valueFactory)
        {
            return ToUInt32(valueFactory());
        }
        public static Maybe<uint> ToUInt32(Func<string> valueFactory, NumberStyles style, IFormatProvider formatProvider)
        {
            return ToUInt32(valueFactory(), style, formatProvider);
        }

        public static Maybe<long> ToInt64(Func<string> valueFactory)
        {
            return ToInt64(valueFactory());
        }
        public static Maybe<long> ToInt64(Func<string> valueFactory, NumberStyles style, IFormatProvider formatProvider)
        {
            return ToInt64(valueFactory(), style, formatProvider);
        }

        public static Maybe<ulong> ToUInt64(Func<string> valueFactory)
        {
            return ToUInt64(valueFactory());
        }
        public static Maybe<ulong> ToUInt64(Func<string> valueFactory, NumberStyles style, IFormatProvider formatProvider)
        {
            return ToUInt64(valueFactory(), style, formatProvider);
        }

        public static Maybe<float> ToSingle(Func<string> valueFactory)
        {
            return ToSingle(valueFactory());
        }
        public static Maybe<float> ToSingle(Func<string> valueFactory, NumberStyles style, IFormatProvider formatProvider)
        {
            return ToSingle(valueFactory(), style, formatProvider);
        }

        public static Maybe<double> ToDouble(Func<string> valueFactory)
        {
            return ToDouble(valueFactory());
        }
        public static Maybe<double> ToDouble(Func<string> valueFactory, NumberStyles style, IFormatProvider formatProvider)
        {
            return ToDouble(valueFactory(), style, formatProvider);
        }

        public static Maybe<decimal> ToDecimal(Func<string> valueFactory)
        {
            return ToDecimal(valueFactory());
        }
        public static Maybe<decimal> ToDecimal(Func<string> valueFactory, NumberStyles style, IFormatProvider formatProvider)
        {
            return ToDecimal(valueFactory(), style, formatProvider);
        }

        public static Maybe<DateTime> ToDateTime(Func<string> valueFactory)
        {
            return ToDateTime(valueFactory());
        }
        public static Maybe<DateTime> ToDateTime(Func<string> valueFactory, DateTimeStyles style, IFormatProvider formatProvider)
        {
            return ToDateTime(valueFactory(), style, formatProvider);
        }
        public static Maybe<DateTime> ToDateTimeExact(Func<string> valueFactory, string format, DateTimeStyles style, IFormatProvider formatProvider)
        {
            return ToDateTimeExact(valueFactory(), format, style, formatProvider);
        }

        public static Maybe<TimeSpan> ToTimeSpan(Func<string> valueFactory)
        {
            TimeSpan v;
            if (TimeSpan.TryParse(valueFactory(), out v))
                return v.ToMaybe();

            return Maybe.Empty<TimeSpan>();
        }
        public static Maybe<TimeSpan> ToTimeSpan(Func<string> valueFactory, IFormatProvider formatProvider)
        {
            return ToTimeSpan(valueFactory(), formatProvider);
        }
        public static Maybe<TimeSpan> ToTimeSpanExact(Func<string> valueFactory, string format, TimeSpanStyles style, IFormatProvider formatProvider)
        {
            return ToTimeSpanExact(valueFactory(), format, style, formatProvider);
        }
        public static Maybe<TimeSpan> ToTimeSpanExact(Func<string> valueFactory, string format, IFormatProvider formatProvider)
        {
            return ToTimeSpanExact(valueFactory(), format, formatProvider);
        }
        public static Maybe<TimeSpan> ToTimeSpanExact(Func<string> valueFactory, string[] formats, TimeSpanStyles style, IFormatProvider formatProvider)
        {
            return ToTimeSpanExact(valueFactory(), formats, style, formatProvider);
        }
        public static Maybe<TimeSpan> ToTimeSpanExact(Func<string> valueFactory, string[] formats, IFormatProvider formatProvider)
        {
            return ToTimeSpanExact(valueFactory(), formats, formatProvider);
        }
    }
}