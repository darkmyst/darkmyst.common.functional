﻿using System;
using System.Collections.Generic;

namespace Darkmyst.Common.Functional
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<TResult> Pull<TResult>(Func<TResult> operation) { while (true) yield return operation(); }
    }
}