using System;
using System.Threading.Tasks;
using Darkmyst.Common.Functional.Results;

namespace Darkmyst.Common.Functional
{
    public static class Handling
    {
        public static TResult Try<TResult>(
            Func<TResult> operation,
            Func<Exception, TResult> handleException)
        {
            try
            {
                return operation();
            }
            catch (Exception ex)
            {
                return handleException(ex);
            }
        }

        public static async Task<TResult> TryAsync<TResult>(
            Func<Task<TResult>> operation,
            Func<Exception, Task<TResult>> handleException)
        {
            try
            {
                return await operation();
            }
            catch (Exception ex)
            {
                return await handleException(ex);
            }
        }
        public static TResult WithCommonHandling<TResult>(
            Func<TResult> operation)
            where TResult : IResult
        {
            try
            {
                return operation();
            }
            catch (Exception ex)
            {
                return Result.CreateErrorResult<TResult>(new Error(ErrorCode.ExceptionThrown, ex, null));
            }
        }
        public static async Task<TResult> WithCommonHandlingAsync<TResult>(
            Func<Task<TResult>> operation)
            where TResult : IResult
        {
            try
            {
                return await operation();
            }
            catch (Exception ex)
            {
                return await Task.FromResult(Result.CreateErrorResult<TResult>(new Error(ErrorCode.ExceptionThrown, ex, null)));
            }
        }
    }
}