using System;
using System.Diagnostics;
using System.Globalization;

namespace Darkmyst.Common.Functional
{
    [DebuggerStepThrough]
    public static partial class ParseMaybe
    {
        /// <summary>
        ///     Tries to convert the specified string representation of a logical value to its <see cref="Boolean"/> equivalent.
        /// </summary>
        /// <param name="input">A string containing the value to convert.</param>
        /// <returns>
        ///     If parsed successfully, a <see cref="Maybe"/> containing true if value is equivalent to <see cref="Boolean.TrueString"/> 
        ///     or false if value is equivalent to <see cref="Boolean.FalseString"/>. Otherwise an empty <see cref="Maybe"/>. 
        /// </returns>
        /// <remarks>
        ///     The conversion fails if value is null or is not equivalent to the value of either the <see cref="Boolean.TrueString"/> 
        ///     or <see cref="Boolean.FalseString"/> field.
        /// </remarks>
        public static Maybe<bool> ToBoolean(string input)
        {
            bool v;
            if (bool.TryParse(input, out v))
                return v.ToMaybe();

            return Maybe.Empty<bool>();
        }

        /// <summary>
        ///     Converts the value of the specified string to its equivalent Unicode character.
        /// </summary>
        /// <param name="input">A string that contains a single character, or null.</param>
        /// <returns>
        ///     If parsed successfully, a <see cref="Maybe"/> containing a Unicode character equivalent to the sole character in <see cref="input"/>. 
        ///     Otherwise an empty <see cref="Maybe"/>.
        /// </returns>
        /// <remarks>The conversion fails if the <see cref="input"/> parameter is null or the length of <see cref="input"/> is not 1.</remarks>
        public static Maybe<char> ToChar(string input)
        {
            char v;
            if (char.TryParse(input, out v))
                return v.ToMaybe();

            return Maybe.Empty<char>();
        }

        /// <summary>
        /// Tries to convert the string representation of a number to its <see cref="SByte"/> equivalent.
        /// </summary>
        /// <param name="input">A string that contains a number to convert.</param>
        /// <returns>
        ///     If parsed successfully, a <see cref="Maybe"/> containing the 8-bit signed integer value that is equivalent to the number contained in <see cref="input"/>. 
        ///     Otherwise an empty <see cref="Maybe"/>. 
        /// </returns>
        /// <remarks>
        ///     The conversion fails if the <see cref="input"/> parameter is null, is not in the correct format, 
        ///     or represents a number that is less than <see cref="SByte.MinValue"/> or greater than <see cref="SByte.MaxValue"/>.
        /// </remarks>
        public static Maybe<sbyte> ToSByte(string input)
        {
            sbyte v;
            if (sbyte.TryParse(input, out v))
                return v.ToMaybe();

            return Maybe.Empty<sbyte>();
        }

        /// <summary> 
        ///     Tries to convert the string representation of a number in a specified style and culture-specific format to its <see cref="SByte"/> equivalent
        /// </summary>
        /// <param name="input">A string that contains a number to convert.</param>
        /// <param name="style">
        ///     A bitwise combination of enumeration values that indicates the permitted format of <see cref="input"/>. A typical value 
        ///     to specify is <see cref="NumberStyles.Integer"/>.
        /// </param>
        /// <param name="formatProvider">An object that supplies culture-specific formatting information about <see cref="input"/>.</param>
        /// <returns>
        ///     If parsed successfully, a <see cref="Maybe"/> containing the 8-bit signed integer value that is equivalent to the number contained in <see cref="input"/>. 
        ///     Otherwise an empty <see cref="Maybe"/>.
        /// </returns>
        /// <remarks> 
        ///     The conversion fails if the <see cref="input"/> parameter is null, is not in the correct format, or represents a number that is less 
        ///     than <see cref="SByte.MinValue"/> or greater than <see cref="SByte.MaxValue"/>.
        /// </remarks>
        public static Maybe<sbyte> ToSByte(string input, NumberStyles style, IFormatProvider formatProvider)
        {
            sbyte v;
            if (sbyte.TryParse(input, style, formatProvider, out v))
                return v.ToMaybe();

            return Maybe.Empty<sbyte>();
        }

        /// <summary>
        /// Tries to convert the string representation of a number to its <see cref="Byte"/> equivalent.
        /// </summary>
        /// <param name="input">A string that contains a number to convert. The string is interpreted using the <see cref="NumberStyles.Integer"/> style.</param>
        /// <returns> 
        ///     If parsed successfully, a <see cref="Maybe"/> containing the <see cref="Byte"/> value that is equivalent to the number contained in <see cref="input"/>. 
        ///     Otherwise an empty <see cref="Maybe"/>.
        /// </returns>
        /// <remarks>
        ///     The conversion fails and the method returns false if the <see cref="input"/> parameter is not in the correct format, if it is null or <see cref="String.Empty"/>, or if it represents a number less than <see cref="Byte.MinValue"/> or greater than <see cref="byte.MaxValue"/>.
        /// </remarks>
        public static Maybe<byte> ToByte(string input)
        {
            byte v;
            if (byte.TryParse(input, out v))
                return v.ToMaybe();

            return Maybe.Empty<byte>();
        }
        public static Maybe<byte> ToByte(string input, NumberStyles style, IFormatProvider formatProvider)
        {
            byte v;
            if (byte.TryParse(input, style, formatProvider, out v))
                return v.ToMaybe();

            return Maybe.Empty<byte>();
        }

        public static Maybe<short> ToInt16(string input)
        {
            short v;
            if (short.TryParse(input, out v))
                return v.ToMaybe();

            return Maybe.Empty<short>();
        }
        public static Maybe<short> ToInt16(string input, NumberStyles style, IFormatProvider formatProvider)
        {
            short v;
            if (short.TryParse(input, style, formatProvider, out v))
                return v.ToMaybe();

            return Maybe.Empty<short>();
        }

        public static Maybe<ushort> ToUInt16(string input)
        {
            ushort v;
            if (ushort.TryParse(input, out v))
                return v.ToMaybe();

            return Maybe.Empty<ushort>();
        }
        public static Maybe<ushort> ToUInt16(string input, NumberStyles style, IFormatProvider formatProvider)
        {
            ushort v;
            if (ushort.TryParse(input, style, formatProvider, out v))
                return v.ToMaybe();

            return Maybe.Empty<ushort>();
        }

        public static Maybe<int> ToInt32(string input)
        {
            int v;
            if (int.TryParse(input, out v))
                return v.ToMaybe();

            return Maybe.Empty<int>();
        }
        public static Maybe<int> ToInt32(string input, NumberStyles style, IFormatProvider formatProvider)
        {
            int v;
            if (int.TryParse(input, style, formatProvider, out v))
                return v.ToMaybe();

            return Maybe.Empty<int>();
        }

        public static Maybe<uint> ToUInt32(string input)
        {
            uint v;
            if (uint.TryParse(input, out v))
                return v.ToMaybe();

            return Maybe.Empty<uint>();
        }
        public static Maybe<uint> ToUInt32(string input, NumberStyles style, IFormatProvider formatProvider)
        {
            uint v;
            if (uint.TryParse(input, style, formatProvider, out v))
                return v.ToMaybe();

            return Maybe.Empty<uint>();
        }

        public static Maybe<long> ToInt64(string input)
        {
            long v;
            if (long.TryParse(input, out v))
                return v.ToMaybe();

            return Maybe.Empty<long>();
        }
        public static Maybe<long> ToInt64(string input, NumberStyles style, IFormatProvider formatProvider)
        {
            long v;
            if (long.TryParse(input, style, formatProvider, out v))
                return v.ToMaybe();

            return Maybe.Empty<long>();
        }

        public static Maybe<ulong> ToUInt64(string input)
        {
            ulong v;
            if (ulong.TryParse(input, out v))
                return v.ToMaybe();

            return Maybe.Empty<ulong>();
        }
        public static Maybe<ulong> ToUInt64(string input, NumberStyles style, IFormatProvider formatProvider)
        {
            ulong v;
            if (ulong.TryParse(input, style, formatProvider, out v))
                return v.ToMaybe();

            return Maybe.Empty<ulong>();
        }

        public static Maybe<float> ToSingle(string input)
        {
            float v;
            if (float.TryParse(input, out v))
                return v.ToMaybe();

            return Maybe.Empty<float>();
        }
        public static Maybe<float> ToSingle(string input, NumberStyles style, IFormatProvider formatProvider)
        {
            float v;
            if (float.TryParse(input, style, formatProvider, out v))
                return v.ToMaybe();

            return Maybe.Empty<float>();
        }

        public static Maybe<double> ToDouble(string input)
        {
            double v;
            if (double.TryParse(input, out v))
                return v.ToMaybe();

            return Maybe.Empty<double>();
        }
        public static Maybe<double> ToDouble(string input, NumberStyles style, IFormatProvider formatProvider)
        {
            double v;
            if (double.TryParse(input, style, formatProvider, out v))
                return v.ToMaybe();

            return Maybe.Empty<double>();
        }

        public static Maybe<decimal> ToDecimal(string input)
        {
            decimal v;
            if (decimal.TryParse(input, out v))
                return v.ToMaybe();

            return Maybe.Empty<decimal>();
        }
        public static Maybe<decimal> ToDecimal(string input, NumberStyles style, IFormatProvider formatProvider)
        {
            decimal v;
            if (decimal.TryParse(input, style, formatProvider, out v))
                return v.ToMaybe();

            return Maybe.Empty<decimal>();
        }

        public static Maybe<DateTime> ToDateTime(string input)
        {
            DateTime v;
            if (DateTime.TryParse(input, out v))
                return v.ToMaybe();

            return Maybe.Empty<DateTime>();
        }
        public static Maybe<DateTime> ToDateTime(string input, DateTimeStyles style, IFormatProvider formatProvider)
        {
            DateTime v;
            if (DateTime.TryParse(input, formatProvider, style, out v))
                return v.ToMaybe();

            return Maybe.Empty<DateTime>();
        }
        public static Maybe<DateTime> ToDateTimeExact(string input, string format, DateTimeStyles style, IFormatProvider formatProvider)
        {
            DateTime v;
            if (DateTime.TryParseExact(input, format, formatProvider, style, out v))
                return v.ToMaybe();

            return Maybe.Empty<DateTime>();
        }

        public static Maybe<TimeSpan> ToTimeSpan(string input)
        {
            TimeSpan v;
            if (TimeSpan.TryParse(input, out v))
                return v.ToMaybe();

            return Maybe.Empty<TimeSpan>();
        }
        public static Maybe<TimeSpan> ToTimeSpan(string input, IFormatProvider formatProvider)
        {
            TimeSpan v;
            if (TimeSpan.TryParse(input, formatProvider, out v))
                return v.ToMaybe();

            return Maybe.Empty<TimeSpan>();
        }
        public static Maybe<TimeSpan> ToTimeSpanExact(string input, string format, TimeSpanStyles style, IFormatProvider formatProvider)
        {
            TimeSpan v;
            if (TimeSpan.TryParseExact(input, format, formatProvider, style, out v))
                return v.ToMaybe();

            return Maybe.Empty<TimeSpan>();
        }
        public static Maybe<TimeSpan> ToTimeSpanExact(string input, string format, IFormatProvider formatProvider)
        {
            TimeSpan v;
            if (TimeSpan.TryParseExact(input, format, formatProvider, out v))
                return v.ToMaybe();

            return Maybe.Empty<TimeSpan>();
        }
        public static Maybe<TimeSpan> ToTimeSpanExact(string input, string[] formats, TimeSpanStyles style, IFormatProvider formatProvider)
        {
            TimeSpan v;
            if (TimeSpan.TryParseExact(input, formats, formatProvider, style, out v))
                return v.ToMaybe();

            return Maybe.Empty<TimeSpan>();
        }
        public static Maybe<TimeSpan> ToTimeSpanExact(string input, string[] formats, IFormatProvider formatProvider)
        {
            TimeSpan v;
            if (TimeSpan.TryParseExact(input, formats, formatProvider, out v))
                return v.ToMaybe();

            return Maybe.Empty<TimeSpan>();
        }
    }
}