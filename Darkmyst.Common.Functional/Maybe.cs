using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;

namespace Darkmyst.Common.Functional
{
    public static class Maybe
    {
        public static Maybe<T> Empty<T>()
        {
#pragma warning disable 618
            return Maybe<T>.Empty;
#pragma warning restore 618
        }
    }
    public struct Maybe<T> : IEnumerable<T>, IEquatable<T>, IEquatable<Maybe<T>>
    {
        private readonly IEnumerable<T> _enumerableValue;
        private IEnumerable<T> EnumerableValue => _enumerableValue ?? Enumerable.Empty<T>();

        public Maybe(T value)
        {
            _enumerableValue = value == null ? Enumerable.Empty<T>() : new[] { value };
        }

        internal static Maybe<T> Empty { get; } = new Maybe<T>();

        public bool HasValue => EnumerableValue.Any();

        public T Value => EnumerableValue.Single();


        public IEnumerator<T> GetEnumerator()
        {
            return EnumerableValue.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public bool Equals(T other)
        {
            return EnumerableValue.Any(x => Equals(x, other));
        }

        public bool Equals(Maybe<T> other)
        {
            if (!EnumerableValue.Any() && !other.Any())
                return true;

            if (!EnumerableValue.Any() || !other.Any())
                return false;

            return Equals(other.Single());
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;

            return obj is Maybe<T> && Equals((Maybe<T>)obj);
        }

        public override int GetHashCode()
        {
            return EnumerableValue.Select(x => x.GetHashCode()).DefaultIfEmpty(Maybe.Empty<T>().GetHashCode()).SingleOrDefault();
        }

        public static bool operator ==(Maybe<T> lhs, Maybe<T> rhs)
        {
            return lhs.Equals(rhs);
        }
        public static bool operator !=(Maybe<T> lhs, Maybe<T> rhs)
        {
            return !Equals(lhs, rhs);
        }
        public static bool operator ==(Maybe<T> lhs, T rhs)
        {
            return lhs.Equals(new Maybe<T>(rhs));
        }
        public static bool operator !=(Maybe<T> lhs, T rhs)
        {
            return !lhs.Equals(new Maybe<T>(rhs));
        }
        public static bool operator ==(Maybe<T> lhs, object rhs)
        {
            return Equals(lhs, rhs);
        }
        public static bool operator !=(Maybe<T> lhs, object rhs)
        {
            return !Equals(lhs, rhs);
        }
        public static implicit operator Maybe<T>(T value)
        {
            return new Maybe<T>(value);
        }
        public static implicit operator T(Maybe<T> value)
        {
            return value.Value;
        }
    }
}