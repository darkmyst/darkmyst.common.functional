namespace Darkmyst.Common.Functional.Results
{
    public enum ErrorCode
    {
        Unknown = 0,
        ExceptionThrown = 1,
        FailedWithoutMessage = 2
    }
}