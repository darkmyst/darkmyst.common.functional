namespace Darkmyst.Common.Functional.Results
{
    public enum ValidationIssueId
    {
        Unknown = 0,
        RequiredValueMissing,
        StringTooShort
    }
}