using System.Collections.Generic;
using System.Linq;

namespace Darkmyst.Common.Functional.Results
{
    public class Result : IResult
    {
        internal Result(IEnumerable<Error> errors,
            IEnumerable<ValidationIssue> validationIssues)
        {
            Errors = errors ?? new List<Error>();
            ValidationIssues = validationIssues ?? new List<ValidationIssue>();
            IsSuccessful = !Enumerable.Any(Errors) && !Enumerable.Any(ValidationIssues);
        }

        public bool IsSuccessful { get; }
        public IEnumerable<Error> Errors { get; }
        public IEnumerable<ValidationIssue> ValidationIssues { get; }

        public static TResult CreateSuccessResult<TResult>()
            where TResult : Result
            => ReflectionHelpers
                .CreateInstanceWithPublicOrNonPublicConstructor<TResult>(null, null);

        public static TResult CreateErrorResult<TResult>(params Error[] errors)
            where TResult : IResult
            => ReflectionHelpers
                .CreateInstanceWithPublicOrNonPublicConstructor<TResult>(errors, null);

        public static TResult CreateValidationIssueResult<TResult>(params ValidationIssue[] validationIssues)
            where TResult : IResult
            => ReflectionHelpers
                .CreateInstanceWithPublicOrNonPublicConstructor<TResult>(null, validationIssues);
    }
}