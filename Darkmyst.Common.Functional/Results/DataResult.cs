using System.Collections.Generic;
using System.Linq;

namespace Darkmyst.Common.Functional.Results
{
    public class DataResult<TData> : Result
    {
        internal DataResult(
            TData data,
            IEnumerable<Error> errors,
            IEnumerable<ValidationIssue> validationIssues)
            : base(errors, validationIssues)
        {
            Data = data?.ToMaybe() ?? Maybe.Empty<TData>();
        }

        internal DataResult(
            Error[] errors,
            IEnumerable<ValidationIssue> validationIssues)
            : base(errors, validationIssues)
        {
            Data = Maybe.Empty<TData>();
        }

        public Maybe<TData> Data { get; }
    }

    public static class DataResult { 
        public static DataResult<TData> CreateSuccessResult<TData>(TData data)
        {
            return new DataResult<TData>(data, null, null);
        }
        public static DataResult<TData> CreateErrorResult<TData>(params Error[] errors)
        {
            return new DataResult<TData>(errors, null);
        }

        public static DataResult<TData> CreateValidationIssueResult<TData>(params ValidationIssue[] validationIssues)
        {
            return new DataResult<TData>(null, validationIssues);
        }
    }
}