using System.Collections.Generic;

namespace Darkmyst.Common.Functional.Results
{
    public interface IResult
    {
        bool IsSuccessful { get; }
        IEnumerable<Error> Errors { get; }
        IEnumerable<ValidationIssue> ValidationIssues { get; }
    }
}