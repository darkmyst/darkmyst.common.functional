﻿using System;
using System.Threading.Tasks;

namespace Darkmyst.Common.Functional
{
    public static class Disposable
    {
        public static TResult Using<TWith, TResult>(
            Func<TWith> usingFactory,
            Func<TWith, TResult> operation)
            where TWith : IDisposable
        {
            using (var with = usingFactory())
            {
                return operation(with);
            }
        }
        public static async Task<TResult> UsingAsync<TWith, TResult>(
            Func<TWith> usingFactory,
            Func<TWith, Task<TResult>> operation)
            where TWith : IDisposable
        {
            using (var with = usingFactory())
            {
                return await operation(with);
            }
        }
    }
}
